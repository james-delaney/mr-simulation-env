import enum
from gym_grid_wireless.node import Node
from gym_grid_wireless.radio import Radio

class Actions(enum.IntEnum):
    RADIO_0_TX_POW_0        = 0
    RADIO_0_TX_POW_1        = 1
    RADIO_0_TX_POW_2        = 2
    RADIO_0_TX_POW_3        = 3
    RADIO_0_TX_POW_4        = 4
    RADIO_1_TX_POW_0        = 5
    RADIO_1_TX_POW_1        = 6
    RADIO_1_TX_POW_2        = 7
    RADIO_1_TX_POW_3        = 8
    RADIO_1_TX_POW_4        = 9
    NO_TX                   = 10

class ActionsSimple(enum.IntEnum):
    RADIO_0_TX_POW_0        = 0
    RADIO_0_TX_POW_1        = 1
    RADIO_0_TX_POW_2        = 2
    RADIO_0_TX_POW_3        = 3
    RADIO_0_TX_POW_4        = 4
    RADIO_1_TX_POW_0        = 5
    RADIO_1_TX_POW_1        = 6
    RADIO_1_TX_POW_2        = 7
    RADIO_1_TX_POW_3        = 8
    RADIO_1_TX_POW_4        = 9

class LowPowerActions(enum.IntEnum):
    RADIO_0_TX_POW_0        = 0
    RADIO_1_TX_POW_0        = 5
    NO_TX                   = 10

class HighPowerActions(enum.IntEnum):
    RADIO_0_TX_POW_4        = 4
    RADIO_1_TX_POW_4        = 9
    NO_TX                   = 10


class Radio0ActionsOnly(enum.IntEnum):
    RADIO_0_TX_POW_0        = 0
    RADIO_0_TX_POW_1        = 1
    RADIO_0_TX_POW_2        = 2
    RADIO_0_TX_POW_3        = 3
    RADIO_0_TX_POW_4        = 4
    NO_TX                   = 10

class Radio1ActionsOnly(enum.IntEnum):
    RADIO_1_TX_POW_0        = 5
    RADIO_1_TX_POW_1        = 6
    RADIO_1_TX_POW_2        = 7
    RADIO_1_TX_POW_3        = 8
    RADIO_1_TX_POW_4        = 9
    NO_TX                   = 10

class ControlActions(enum.IntEnum):
    RADIO_OFF           = 0
    RADIO_PWR_INC       = 1
    RADIO_PWR_DEC       = 2
    RADIO_PWR_STB       = 3
    RADIO_SWITCH        = 4