
from gym.spaces import Tuple, Discrete, Dict
import numpy as np


class PktSimCfg():
    
    def __init__(self, name, idx):
        self.name = name
        self.idx = idx
        self.cfg = {}
        
    def __str__(self):
        return self.name

    def __float__(self):
        return float(self.idx)

    def dict(self):
        return self.cfg
    
    
class DefaultPktSimCfg(PktSimCfg):
    
    def __init__(self):
        super().__init__('default_pkt_sim_cfg', 0)
        self.cfg = {
            'pkt_size':10000,             # bytes
            'interval':0.2,               # seconds
            'err_threshold':0.0            # % of errored bytes in pkt before it is discarded
        }

class HighDrPktSimCfg(PktSimCfg):
    
    def __init__(self):
        super().__init__('default_pkt_sim_cfg', 1)
        self.cfg = {
            'pkt_size':10000,
            'interval':0.2,
            'err_threshold':0.0
        }
        
class LowDrPktSimCfg(PktSimCfg):
    
    def __init__(self):
        super().__init__('default_pkt_sim_cfg', 2)
        self.cfg = {
            'pkt_size':100,
            'interval':1,
            'err_threshold':0.0
        }

class HighFreqPktSimCfg(PktSimCfg):
    
    def __init__(self):
        super().__init__('default_pkt_sim_cfg', 3)
        self.cfg = {
            'pkt_size':10000,             # bytes
            'interval':0.2,               # seconds
            'err_threshold':0.0            # % of errored bytes in pkt before it is discarded
        }

class LowFreqPktSimCfg(PktSimCfg):
    
    def __init__(self):
        super().__init__('default_pkt_sim_cfg', 4)
        self.cfg = {
            'pkt_size': 100,
            'interval': 5,
            'err_threshold':0.0    
        }


class Node():
    
    def __init__(self, x_pos, y_pos, radios):
        self.x_pos = x_pos
        self.y_pos = y_pos

        self.num_radios = len(radios)
        self.radios = radios
        self.curr_radio = 0
        self.per = 0

        self.pkt_sim = DefaultPktSimCfg()
        self.pkt_sim_cfg = self.pkt_sim.dict()
        
        self.pkt_sim_trace = [0,0,0,0,0,0,0,-1,0]                 # [status, transmitted, lost, current pkt id, remaining bytes, lost bits, radio of tx, tx start time,avg goodput]
                                                         # "status" is 0 when not currently transmitting, 1 when transmitting

        self.rssi_wnd = 1000
        self.inst_rssi = [[] for i in range(self.num_radios)]
        self.avg_rssi = [0 for i in range(self.num_radios)]

    def get_x_pos(self):
        return self.x_pos

    def set_x_pos(self, x_pos):
        self.x_pos = x_pos

    def get_y_pos(self):
        return self.y_pos

    def set_y_pos(self, y_pos):
        self.y_pos = y_pos

    def get(self):
        return [self.x_pos, self.y_pos]

    def get_radios(self):
        return self.radios

    def get_curr_radio_id(self):
        return self.curr_radio
    
    def set_curr_radio_id_and_tx_pow(self, radio, tx_pow_step):
        self.curr_radio = radio
        self.radios[self.curr_radio].set_tx_pow(tx_pow_step)

    def update_radio_dst_stats(self, radio_id, rssi, fade_margin):
        
        '''
        #print(self.inst_rssi[1])
        #print(len(self.inst_rssi[0]), len(self.inst_rssi[1]))
        if len(self.inst_rssi[radio_id]) < self.rssi_wnd:
            self.inst_rssi[radio_id].append(rssi)
            self.avg_rssi[radio_id] = np.mean(self.inst_rssi[radio_id])
        else:
            n0 = self.inst_rssi[radio_id].pop(0)
            self.inst_rssi[radio_id].append(rssi)
            #mean_inc = (rssi - n0) / self.rssi_wnd
            #self.avg_rssi[radio_id] = self.avg_rssi[radio_id] + mean_inc
            self.avg_rssi[radio_id] = np.mean(self.inst_rssi[radio_id])
            
        self.radios[radio_id].set_RSSI_to_dst(self.avg_rssi[radio_id])
        #self.radios[radio_id].set_fade_margin_to_dst(fade_margin)
        self.radios[radio_id].set_fade_margin_to_dst(self.avg_rssi[radio_id] - self.radios[radio_id].get_rx_sensitivity())
        '''
        
        
        self.radios[radio_id].set_RSSI_to_dst(rssi)
        self.radios[radio_id].set_fade_margin_to_dst(fade_margin)
        
        
    def update_radio_shd_sigma(self, radio_id, shd):
        self.radios[radio_id].set_shd_sigma(shd)

    def update_radio_noise(self, radio_id, noise):
        self.radios[radio_id].set_noise(noise)

    def __str__(self):
        return str(self.x_pos) + ", " + str(self.y_pos)

    def get_per(self):
        return self.per

    def step_pkt_sim(self, time_interval, time_step):
        curr_time_epoch = time_step * time_interval
        status = self.pkt_sim_trace[0]
        num_pkts_tx = self.pkt_sim_trace[1]
        num_pkts_lost = self.pkt_sim_trace[2]
        curr_pkt_id = self.pkt_sim_trace[3]
        curr_pkt_rem_bytes = self.pkt_sim_trace[4]
        curr_pkt_lost_bits = self.pkt_sim_trace[5]
        curr_pkt_radio = self.pkt_sim_trace[6]
        last_pkt_tx_time = self.pkt_sim_trace[7]
        goodput = self.pkt_sim_trace[8]
        
        pkt_lost = False
        #print('%.1f' % curr_time_epoch)
        if status:                                                                  # if transmitting

            if curr_pkt_radio is not self.curr_radio:
                #print('%.1f radio change loss' % curr_time_epoch )
                status = 0
                num_pkts_lost += 1
                curr_pkt_lost_bits = 0
                #if (curr_pkt_radio == 0):
                    #print(self.x_pos, self.y_pos, curr_pkt_id)
            else:

                if curr_pkt_rem_bytes <= 0:                                                      # if no more bytes to send, change status to 0 (not transmitting)
                                                                       # increment no. of transmitted packets
                    # if above the threshold of errored bits in a transmission - we lost a packet                                                      
                    if curr_pkt_lost_bits/(self.pkt_sim_cfg['pkt_size'] * 8) > self.pkt_sim_cfg['err_threshold']:
                        num_pkts_lost += 1                                              # lost pkt
                        pkt_lost = True
                        #print(self.x_pos, self.y_pos, self.curr_radio)

                    status = 0
                    num_pkts_tx += 1 
                    curr_pkt_lost_bits = 0                                                      # reset count of lost bits
                else:    
                    #rem_bytes = self.pkt_sim_trace[4]                                          # get remaining bytes
                    ppi = np.floor(((self.radios[self.curr_radio].bit_rate/8) * time_interval)/self.radios[self.curr_radio].phy_mtu) # packets per interval of time
                    chunk_bytes = ppi * self.radios[self.curr_radio].phy_mtu     # no. of bytes to send in this time interval
                    #print(chunk_bytes, ppi, self.radios[self.curr_radio].bit_rate)
                    if (curr_pkt_rem_bytes <= chunk_bytes):                                     # if we can tx all remaining bytes in this interval
                        chunk_bytes = curr_pkt_rem_bytes                                        #  then queue all of them to send in chunk_bytes
                        curr_pkt_rem_bytes = 0
                        num_pkts_tx += 1                                                        #  increment tx counter 
                        status = 0                                                              #  set status to 0 (not transmitting)
                    else:
                        curr_pkt_rem_bytes -= chunk_bytes                                       # record remaining bytes if we can only send less than the size of the remaining pkts
                    #print(chunk_bytes)
                    num_phy_tx, num_phy_tx_lost = self.radios[self.curr_radio].sim_pkt_data(chunk_bytes)
                    curr_pkt_lost_bits += num_phy_tx_lost # simulate transmission on current radio
                    
                    #self.pkt_sim_trace[5] += lost_bits
                    #print(curr_pkt_rem_bytes)
                    #curr_pkt_lost_bits += lost_bits
                    
                    #goodput += ((num_phy_tx-num_phy_tx_lost)*self.radios[self.curr_radio].phy_mtu)
                    goodput += ((num_phy_tx-num_phy_tx_lost))*self.pkt_sim_cfg['pkt_size']

                

        else:                                                                       # if not transmitting
            if ((curr_time_epoch - last_pkt_tx_time) >= self.pkt_sim_cfg['interval']):
                #print('%.1f' % curr_time_epoch)
                #print('curr time:' + str(curr_time_epoch) + ' last pkt time: ' + str(last_pkt_tx_time))
                if (self.radios[self.curr_radio].get_link_status()):
                    last_pkt_tx_time = curr_time_epoch
                    status = 1                                                              # change status to 1 (transmitting)
                    curr_pkt_radio = self.curr_radio
                    curr_pkt_id += 1                                                        # increment current pkt id counter
                    curr_pkt_rem_bytes = self.pkt_sim_cfg['pkt_size']                       # record no. of bytes remaining
                    ppi = np.floor(((self.radios[self.curr_radio].bit_rate/8) * time_interval)/self.radios[self.curr_radio].phy_mtu) # packets per interval of time
                    chunk_bytes = ppi * self.radios[self.curr_radio].phy_mtu     # no. of bytes to send in this time interval
                    if (curr_pkt_rem_bytes <= chunk_bytes):                                     # if we can tx all remaining bytes in this interval
                        chunk_bytes = curr_pkt_rem_bytes                                        #  then queue all of them to send in chunk_bytes
                        curr_pkt_rem_bytes = 0
                        
                        num_phy_tx, num_phy_tx_lost = self.radios[self.curr_radio].sim_pkt_data(chunk_bytes)
                        curr_pkt_lost_bits += num_phy_tx_lost
                        #if (curr_pkt_lost_bits > 0):
                            #print(curr_pkt_lost_bits)
                        if (curr_pkt_lost_bits/(self.pkt_sim_cfg['pkt_size'] * 8)) > self.pkt_sim_cfg['err_threshold']:
                            #print(curr_pkt_lost_bits, self.curr_radio)
                            num_pkts_lost += 1                                              # lost pkt
                            pkt_lost = True
                            #print('err th pkt lost', self.x_pos, self.y_pos, curr_pkt_radio)

                        num_pkts_tx += 1
                        status = 0
                        curr_pkt_lost_bits = 0
                        #print(goodput, tx_bits, lost_bits)
                        #goodput += ((num_phy_tx-num_phy_tx_lost)*self.radios[self.curr_radio].phy_mtu)
                        goodput += ((num_phy_tx-num_phy_tx_lost))*self.pkt_sim_cfg['pkt_size']

                    else:
                        curr_pkt_rem_bytes -= chunk_bytes                                       # record remaining bytes if we can only send less than the size of the remaining pkts
                        num_phy_tx, num_phy_tx_lost = self.radios[self.curr_radio].sim_pkt_data(chunk_bytes)
                        curr_pkt_lost_bits += num_phy_tx_lost
                        #goodput += ((num_phy_tx-num_phy_tx_lost)*self.radios[self.curr_radio].phy_mtu)
                        goodput += ((num_phy_tx-num_phy_tx_lost))*self.pkt_sim_cfg['pkt_size']
                else:
                    last_pkt_tx_time = curr_time_epoch
                    curr_pkt_id += 1
                    num_pkts_tx += 1
                    num_pkts_lost += 1
                    pkt_lost = True

                    status = 0
                    curr_pkt_rem_bytes = 0
                    curr_pkt_lost_bits = 0
                    curr_pkt_radio = self.curr_radio
                    #print('r dn pkt lost ' + str(self.get_curr_radio_id()))

        #self.per = 1 - np.power((1-self.radios[self.curr_radio].get_bit_error_rate()),self.pkt_sim_cfg['pkt_size'])
        #print(self.per)


        self.pkt_sim_trace = [status, num_pkts_tx, num_pkts_lost, curr_pkt_id, curr_pkt_rem_bytes, curr_pkt_lost_bits, curr_pkt_radio, last_pkt_tx_time, goodput]
        return pkt_lost

