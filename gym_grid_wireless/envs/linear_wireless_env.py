import gym
from gym import error, spaces, utils
from gym.utils import seeding
from gym_grid_wireless.node import Node
from gym_grid_wireless.radio import Radio
import numpy as np
import sys
from six import StringIO
from scipy.spatial import distance
from scipy import constants as const
from gym_minigrid.minigrid import *


CELL_PIXELS = 2       # reduce the cell size for each grid

SPACE_W         = 300
SPACE_H         = 300
BOUNDARY        = 25
LEFT_BOUND      = BOUNDARY
RIGHT_BOUND     = SPACE_W - BOUNDARY
TOP_BOUND       = SPACE_H - BOUNDARY
BOTTOM_BOUND    = BOUNDARY

RADIO_CFG = [   Radio(0, 915, 2, -119, 200, [7,15,18,21,24], [4,27,54,107,215]), 
                Radio(1, 2400, 10, -89, 1000, [1,5,11,15,19.5], [1.8,7.8,31,83,240])]
D_0             = 1     # d_nought parameter (reference distance)
PLE             = 3.5  # Path Loss Exponent
PL_SIGMA        = 1     # Shadowing component of path loss

PLE_min         = 1.0
PLE_max         = 5.5

### Action Space Constants
USE_RADIO_0 = 0
USE_RADIO_1 = 1
#NOP         = 2     ## stop tx


### Mobility Model Constants
ALPHA = 0.5
MEAN_SPEED = 15
MEAN_DIR  = const.pi/2
### 
LINEAR_DIR = 1 # +ve is right, -ve is left
MIN_DIST = 1
MAX_DIST = 750

class LinearWirelessEnv(gym.Env):
    metadata = {'render.modes' : ['human']}


    

    def __init__(self, update_interval, god_agent=False):
        self.src_node = Node(1,1,RADIO_CFG)
        self.dst_node = Node(0,0,RADIO_CFG)

        self.action_space = spaces.Discrete(2)

        self.grid_render = None
        
        self.src_node_speed = 0
        self.src_node_dir = 0
        self.alpha = 0

        if (god_agent == True):
            self.god_agent = True
        else:
            self.god_agent = False

        
        self.time_int = 1 #time interval in seconds
        self.time_int_steps = self.time_int/update_interval
        
        self.steps_since_int = 0


    def step(self, action):
        global LINEAR_DIR, PLE

        src_node_x = self.src_node.get_x_pos()
        src_node_y = self.src_node.get_y_pos()
        
        if (self.src_node.get_curr_radio_id() != action):
            radio_changed = 1
        else:
            radio_changed = 0

        if (action == USE_RADIO_0):
            self.src_node.set_curr_radio_id(0)
        elif (action == USE_RADIO_1):
            self.src_node.set_curr_radio_id(1)
        else:
            pass

        # loop over each radio, calculate P_r (received power) w/respect to  destination node
        #PLE = np.random.normal(2.7, 0.3)
        for radio in self.src_node.get_radios():
            
            if (PLE <= PLE_min+0.05):
                PLE = PLE + np.random.normal(0.5, 0.05)
            elif (PLE >= PLE_max-0.05):
                PLE = PLE - np.random.normal(0.5, 0.05)
            else:
                PLE = PLE + np.random.normal(0,0.05)
            

            dist = distance.euclidean(self.src_node.get(), self.dst_node.get())
            lm = const.speed_of_light/(radio.get_freq()*np.power(10,6))
            K = 20*np.log10(lm/(4*const.pi*D_0))
            shadowing = np.random.normal(0, PL_SIGMA)
            P_r = int(radio.get_tx_power() + K - (10 * PLE * np.log10(dist/D_0)) - shadowing)
            fade_margin = P_r - radio.get_rx_sensitivity()
            self.src_node.update_radio_dst_stats(radio.get_radio_id(), P_r, fade_margin)
            #sys.stdout.write("PLE: " + str(PLE) + "radio: " + str(radio.get_freq()) + "\tP_r=" + str(P_r) + "dBm\tMargin=" + str(fade_margin) + "\n")
        


        '''
        dx = np.random.randint(-1,2)
        dy = np.random.randint(-1,2)
        #sys.stdout.write("dx: " + str(dx) + " dy: " + str(dy) + "\n")
        self.src_node.set_x_pos(src_node_x + dx)
        self.src_node.set_y_pos(src_node_y + dy)
        '''
        ### Gauss-Markov mobility for src node
        '''
        
        
        if (self.src_node_speed == 0):
            self.src_node_speed = self._calc_speed(self.src_node_speed)

        if (self.src_node_dir == 0):
            self.src_node_dir = self._calc_dir(src_node_x, src_node_y, self.src_node_dir)
        
        #sys.stdout.write("s: " + str(self._calc_speed(self.src_node_speed)) + " d: " + str(self.src_node_dir) + "\n")
        if (self.steps_since_int >= self.time_int_steps):
            self.src_node_speed = self._calc_speed(self.src_node_speed)
            self.src_node_dir = self._calc_dir(src_node_x, src_node_y, self.src_node_dir)
            #sys.stdout.write("s: " + str(self.src_node_speed) + " d: " + str(self.src_node_dir) + "\n")
            self.steps_since_int = 0
        
        if (self._check_bounds(src_node_x, src_node_y)):
            self.src_node_dir = self._calc_dir(src_node_x, src_node_y, self.src_node_dir)


        #sys.stdout.write("s: " + str(self.src_node_prev_speed) + " d: " + str(self.src_node_prev_dir) + "\n")
        new_x = src_node_x + (self.src_node_speed * np.cos(self.src_node_dir))
        new_y = src_node_y + (self.src_node_speed * np.sin(self.src_node_dir))

        self.steps_since_int = self.steps_since_int + 1

        self.src_node.set_x_pos(new_x)
        self.src_node.set_y_pos(new_y)
        '''

        ### Linear max-distance mobility
        curr_dist = self.src_node.get_x_pos()
        if (curr_dist <= MIN_DIST and LINEAR_DIR == -1):
            LINEAR_DIR = 1
        elif (curr_dist >= MAX_DIST and LINEAR_DIR == 1):
            LINEAR_DIR = -1
        else:
            LINEAR_DIR = np.random.randint(-1,1)
        
        new_x = curr_dist + LINEAR_DIR
        self.src_node.set_x_pos(new_x)

        #print("dist: " + str(new_x))

        curr_radio = self.src_node.get_radios()[self.src_node.get_curr_radio_id()]

        if (self.god_agent == True):
            obs = self.src_node.get_radios()
        else:
            obs = curr_radio
        
        fade_margin = [0,0]

        for i in range(self.src_node.get_radios().__len__()):
            fade_margin[i] = self.src_node.get_radios()[i].get_fade_margin_to_dst()

        '''
        if (fade_margin[curr_radio.get_radio_id()] <= 0):
            reward = -2
        elif (curr_radio.get_radio_id() == 0) and (fade_margin[0] > 0 and fade_margin[1] > 0):
            reward = -2
        elif (curr_radio.get_radio_id() == 1) and (fade_margin[0] > 0 and fade_margin[1] > 0):
            reward = 2
        elif (curr_radio.get_fade_margin_to_dst() > 0):
            reward = 1
        else:
            reward = 0
        '''

        reward = 0

        if (radio_changed):
            if (curr_radio.get_radio_id() == 0):
                if (fade_margin[0] > 0 and fade_margin[1] > 0):
                    reward = -2
                elif (fade_margin[0] > 0 and fade_margin[1] <= 0):
                    reward = 2
                else:
                    reward = 0
            elif (curr_radio.get_radio_id() == 1):
                if (fade_margin[0] <= 0 and fade_margin[1] > 0):
                    reward = 2
                elif (fade_margin[0] > 0 and fade_margin[1] <= 0):
                    reward = -2
                elif (fade_margin[0] > 0 and fade_margin[1] > 0):
                    reward = 2
                else:
                    reward = 0
        else:
            if (curr_radio.get_radio_id() == 0):
                if (fade_margin[0] > 0 and fade_margin[1] > 0):
                    reward = -2
                elif (fade_margin[0] > 0 and fade_margin[1] <= 0):
                    reward = 1
            elif (curr_radio.get_radio_id() == 1):
                if (fade_margin[0] <= 0 and fade_margin[1] > 0):
                    reward = 1
                elif (fade_margin[0] > 0 and fade_margin[1] <= 0):
                    reward = -2


        done = False
        return obs, reward, done, {}

    def reset(self):
        self.src_node.set_x_pos(np.random.randint((SPACE_W/2)+1, SPACE_W))
        self.src_node.set_y_pos(np.random.randint((SPACE_H/2)+1, SPACE_H))
        #self.dst_node.set_x_pos(np.random.randint(0, SPACE_W))
        #self.dst_node.set_y_pos(np.random.randint(0, SPACE_H))
        self.dst_node.set_x_pos(SPACE_W/2)
        self.dst_node.set_y_pos(SPACE_H/2)

        self.grid = Grid(SPACE_W, SPACE_H)

    def render(self, mode='human'):
        '''
        if self.grid_render is None or self.grid_render.window is None or (self.grid_render.width != SPACE_W * CELL_PIXELS):
            from gym_minigrid.rendering import Renderer
            self.grid_render = Renderer(
                SPACE_W * CELL_PIXELS,
                SPACE_H * CELL_PIXELS,
                True if mode == 'human' else False
            )        

        r = self.grid_render

        if (r.window):
            r.window.setText("Wireless Grid")

        r.beginFrame()

        # draw grid
        self.grid.render(r, CELL_PIXELS)

        #draw src node
        r.push()
        #r.translate(CELL_PIXELS*(self.src_node + 0.5), CELL_PIXELS*(self.src_node + 0.5))
        r.setLineColor(255,0,0)
        r.setColor(255,0,0)
        r.drawCircle(CELL_PIXELS*(self.src_node.get_x_pos() + 0.5), CELL_PIXELS*(self.src_node.get_y_pos() + 0.5), 8)
        r.pop()

        #draw dst node
        r.push()
        #r.translate(CELL_PIXELS*(self.dst_node + 0.5), CELL_PIXELS*(self.dst_node + 0.5))
        r.setLineColor(0,0,255)
        r.setColor(0,0,255)
        r.drawCircle(CELL_PIXELS*(self.dst_node.get_x_pos() + 0.5), CELL_PIXELS*(self.dst_node.get_y_pos() + 0.5), 8)
        r.pop()

        r.endFrame()
        
        outfile = StringIO() if mode == 'ansi' else sys.stdout

        outfile.write("Dest node pos: " + str(self.dst_node) + "\n")
        outfile.write("Src node pos: " + str(self.src_node) + "\n")
        
        '''
        return 

    def close(self):
        pass


    def _calc_speed(self, speed):
        new_speed = ((ALPHA * speed) + ((1.0 - ALPHA) * MEAN_SPEED) +  (np.sqrt(1.0 - np.power(ALPHA, 2)) *  np.random.normal(0,1)))
        return new_speed/self.time_int_steps

    def _calc_dir(self, curr_x, curr_y, dir):

        loc_dir = MEAN_DIR

        if ((curr_x <= LEFT_BOUND) and (curr_y <= BOTTOM_BOUND)):
            loc_dir = const.pi/4
        elif ((curr_x <= LEFT_BOUND) and (curr_y >= TOP_BOUND)):
            loc_dir = (7 * const.pi)/4
        elif ((curr_x >= RIGHT_BOUND) and (curr_y >= TOP_BOUND)):
            loc_dir = (5 * const.pi)/4
        elif ((curr_x >= RIGHT_BOUND) and (curr_y <= BOTTOM_BOUND)):
            loc_dir = (3 * const.pi)/4
        elif (curr_x <= LEFT_BOUND):
            loc_dir = 2*const.pi
        elif (curr_x >= RIGHT_BOUND):
            loc_dir = const.pi
        elif (curr_y >= TOP_BOUND):
            loc_dir = (3*const.pi)/2
        elif (curr_y <= BOTTOM_BOUND):
            loc_dir = const.pi/2

        return ((ALPHA * dir) + ((1.0 - ALPHA) * loc_dir) + (np.sqrt(1.0 - np.power(ALPHA, 2)) * np.random.normal(0,1)))

    def _check_bounds(self, curr_x, curr_y):
        if ((curr_x <= LEFT_BOUND) and (curr_y <= BOTTOM_BOUND)):
            return True
        elif ((curr_x <= LEFT_BOUND) and (curr_y >= TOP_BOUND)):
            return True
        elif ((curr_x >= RIGHT_BOUND) and (curr_y >= TOP_BOUND)):
            return True
        elif ((curr_x >= RIGHT_BOUND) and (curr_y <= BOTTOM_BOUND)):
            return True
        elif (curr_x <= LEFT_BOUND):
            return True
        elif (curr_x >= RIGHT_BOUND):
            return True
        elif (curr_y >= TOP_BOUND):
            return True
        elif (curr_y <= BOTTOM_BOUND):
            return True
        else:
            return False