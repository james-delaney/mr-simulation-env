import gym
from gym import error, spaces, utils
from gym.utils import seeding
from gym_grid_wireless.node import Node
from gym_grid_wireless.radio import Radio
import numpy as np
import sys
import os
import csv
from scipy.spatial import distance
from scipy import constants as const
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import gridspec

FILE_DIR = os.path.dirname(__file__) + '/'

SPACE_W         = 1000
SPACE_H         = 1000

DST_NODE_X      = SPACE_W/2
DST_NODE_Y      = SPACE_H/2

RADIO_CFG = [   Radio(0, 915, 2, -119, 200, [7,15,18,21,24], [4,27,54,107,215]), 
                Radio(1, 2400, 10, -89, 1000, [1,5,11,15,19.5], [1.8,7.8,31,83,240])]
D_0             = 1     # d_nought parameter (reference distance)
PL_SIGMA        = 2  # Shadowing component of path loss

### Action Space Constants
USE_RADIO_0 = 0
USE_RADIO_1 = 1
NOP         = 2     ## no tx, stay quiet


PATH_POINTS = []

def PLE_for_point(PLE, polygons, point):
    for p in polygons:
        if p.contains(point):
            return PLE[polygons.index(p)]
    
    return 3.5


class PathWirelessEnv(gym.Env):
    metadata = {'render.modes' : ['human']}


    def __init__(self, update_interval, god_agent=False):
        print('init path wireless env')
        self.src_node = Node(0,0,RADIO_CFG)
        self.dst_node = Node(int(DST_NODE_X),int(DST_NODE_Y),RADIO_CFG)

        self.action_space = spaces.Discrete(3)

        self.grid_render = None

        if (god_agent == True):
            self.god_agent = True
        else:
            self.god_agent = False

        
        self.time_int = 1 #time interval in seconds
        self.time_int_steps = self.time_int/update_interval
        
        self.steps_since_int = 0

        self.env_polygons = []
        self.env_PLE = []
        self.path_points = []
        self.path_count = 0
        self.path_len = 0

        self.last_PLE = 0
        self.total_steps = 0

    def step(self, action):
        src_node_x = self.src_node.get_x_pos()
        src_node_y = self.src_node.get_y_pos()
        
        if (self.src_node.get_curr_radio_id() != action):
            radio_changed = 1
        else:
            radio_changed = 0

        if (action == USE_RADIO_0):
            self.src_node.set_curr_radio_id(0)
        elif (action == USE_RADIO_1):
            self.src_node.set_curr_radio_id(1)
        else:
            pass

        
        PLE = PLE_for_point(self.env_PLE, self.env_polygons, Point(src_node_x, src_node_y))
        #PLE = 2
        #print(Point(src_node_x, src_node_y))
        if PLE != self.last_PLE:
            print("PLE changed: " + str(self.total_steps) + ", " + str(PLE))

        dist = distance.euclidean(self.src_node.get(), self.dst_node.get())
        #shadowing = np.random.normal(0, PL_SIGMA)
        shadowing = 0
        for radio in self.src_node.get_radios():
            
            lm = const.speed_of_light/(radio.get_freq()*np.power(10,6))
            K = 20*np.log10(lm/(4*const.pi*D_0))
            
            P_r = int(radio.get_tx_power() + K - (10 * PLE * np.log10(dist/D_0)) - shadowing)
            fade_margin = P_r - radio.get_rx_sensitivity()
            self.src_node.update_radio_dst_stats(radio.get_radio_id(), P_r, fade_margin)
            #sys.stdout.write("radio: " + str(radio.get_freq()) + "\tP_r=" + str(P_r) + "dBm\tMargin=" + str(fade_margin) + "\n")

        self.last_PLE = PLE

        #if action == NOP:
        #    curr_radio = None
        #else:
        curr_radio = self.src_node.get_radios()[self.src_node.get_curr_radio_id()]

        if (self.god_agent == True):
            obs = self.src_node.get_radios()
        else:
            obs = curr_radio
        
        fade_margin = [0,0]

        for i in range(self.src_node.get_radios().__len__()):
            fade_margin[i] = self.src_node.get_radios()[i].get_fade_margin_to_dst()
        
        #print(fade_margin)
        #print(self.src_node.get_curr_radio_id())

        reward = 0

        if (curr_radio is not None):
            if (radio_changed):
                if (action == NOP):
                    if (fade_margin[0] <= 0 and fade_margin[1] <= 0):
                        reward = 2
                    elif (fade_margin[0] > 0 or fade_margin[1] > 0):
                        reward = -10
                    elif (fade_margin[0] > 0 and fade_margin[1] < 0):
                        reward = -10
                elif (curr_radio.get_radio_id() == 0):
                    if (fade_margin[0] > 0 and fade_margin[1] > 0):
                        reward = -5
                        #print("reward 1")
                    elif (fade_margin[0] > 0 and fade_margin[1] <= 0):
                        reward = 2
                        #print("reward 2")
                    else:
                        #print(fade_margin[0], fade_margin[1])
                        reward = -10
                elif (curr_radio.get_radio_id() == 1):
                    if (fade_margin[0] <= 0 and fade_margin[1] > 0):
                        reward = 2
                        #print("reward 3")
                    elif (fade_margin[0] > 0 and fade_margin[1] <= 0):
                        reward = -10
                        #print("reward 4")
                    elif (fade_margin[0] > 0 and fade_margin[1] > 0):
                        reward = 2
                        #print("reward 5")
                    else:
                        #print(fade_margin[0], fade_margin[1])
                        reward = -10
                
            else:
                if (action == NOP):
                    if (fade_margin[0] <= 0 and fade_margin[1] <= 0):
                        reward = 2
                    elif (fade_margin[0] > 0 or fade_margin[1] > 0):
                        reward = -10
                    elif (fade_margin[0] > 0 and fade_margin[1] < 0):
                        reward = -10
                elif (curr_radio.get_radio_id() == 0):
                    if (fade_margin[0] > 0 and fade_margin[1] > 0):
                        reward = -5
                        #print("reward 6")
                    elif (fade_margin[0] > 0 and fade_margin[1] <= 0):
                        reward = 2
                        #print("reward 7")
                    elif (fade_margin[0] <= 0 and fade_margin[1] <= 0):
                        reward = -10
                elif (curr_radio.get_radio_id() == 1):
                    if (fade_margin[0] <= 0 and fade_margin[1] > 0):
                        reward = 2
                        #print("reward 8")
                    elif (fade_margin[0] > 0 and fade_margin[1] > 0):
                        reward = 2
                        #print("reward 9")
                    elif (fade_margin[0] > 0 and fade_margin[1] <= 0):
                        reward = -10
                        #print("reward 10")
                    elif (fade_margin[0] <= 0 and fade_margin[1] <= 0):
                        reward = -10
                
        else:
            if (action == NOP):
                if (fade_margin[0] <= 0 and fade_margin[1] <= 0):
                    reward = 2
                elif (fade_margin[0] > 0 and fade_margin[1] <= 0):
                    reward = -10
                elif (fade_margin[0] > 0 or fade_margin[1] > 0):
                    reward = -10


        self.path_count += 1
        
        
        if (self.path_count >= self.path_len):
            done = True
        else:
            self.src_node.set_x_pos(self.path_points[self.path_count][0])
            self.src_node.set_y_pos(self.path_points[self.path_count][1])
            done = False
        
        self.total_steps += 1

        return obs, reward, done, {}

        


    def reset(self):
        self.path_points = []
        self.path_len = 0
        self.env_polygons = []
        self.env_PLE = []

        self.path_points_x = []
        self.path_points_y = []

        print('loading path')
        with open(FILE_DIR+'a.csv', 'r', newline='') as file:
            lines = csv.reader(file, delimiter=',')
            for line in lines:
                #print([int(line[0]), int(line[1])])
                self.path_points.append([int(line[0]), int(line[1])])
                self.path_points_x.append(int(line[0]))
                self.path_points_y.append(int(line[1]))
        
        
        print('loading polygons')
        

        with open(FILE_DIR+'polys_1.5_4.5.csv', 'r') as file:
            lines = csv.reader(file, delimiter=',')
            env = next(lines)
            
            line=next(lines)
            while line:
            #for line in lines:
                p = []
                
                for i in range(1, len(line), 2):
                    p.append([float(line[i]), float(line[i+1])])
                poly  = Polygon(p)
                
                self.env_polygons.append(poly)
                self.env_PLE.append(float(line[0]))

                line = next(lines, None)
            
            # Comment out plot of environment and path
            '''
            fig = plt.figure(1)
            gs = gridspec.GridSpec(16, 10)

            ax = plt.subplot2grid((16,20), (0,17), colspan=1, rowspan=16)
            ax2 = plt.subplot2grid((16,20), (0,0), colspan=16, rowspan=16)
        
            norm = mpl.colors.Normalize(vmin=1.5, vmax=6.0, clip=False)
            mapper = cm.ScalarMappable(norm=norm, cmap=cm.viridis)
            for p in self.env_polygons:
                ax2.fill(*p.exterior.xy, edgecolor="red",facecolor=mapper.to_rgba(self.env_PLE[self.env_polygons.index(p)]))


            cb1 = mpl.colorbar.ColorbarBase(ax, cmap=cm.viridis, norm=norm, orientation='vertical')
            cb1.set_label('Path Loss Exponent')
            mpl.rcParams.update({'font.size': 15})


            ax2.plot(self.path_points_x, self.path_points_y)

            plt.xlim(0, 1000)
            plt.ylim(0, 1000)
            plt.axis([0,1000,0,1000])
            plt.show()
            '''
            ##


        self.src_node.set_x_pos(self.path_points[0][0])
        self.src_node.set_y_pos(self.path_points[0][1])

        self.dst_node.set_x_pos(SPACE_W/2)
        self.dst_node.set_y_pos(SPACE_H/2)

        self.path_len = len(self.path_points)
        self.path_count += 1

        print('total path length: ' + str(self.path_len) + ' steps')
    
    def render(self, mode='human'):
        return
    

    def close(self):
        pass