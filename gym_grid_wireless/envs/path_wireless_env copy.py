import gym
from gym import error, spaces, utils
from gym.utils import seeding
from gym_grid_wireless.node import Node
from gym_grid_wireless.radio import Radio
import numpy as np
import sys
import os
import csv
from scipy.spatial import distance
from scipy import constants as const
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.cm as cm

FILE_DIR = os.path.dirname(__file__) + '/'

SPACE_W         = 1000
SPACE_H         = 1000

DST_NODE_X      = SPACE_W/2
DST_NODE_Y      = SPACE_H/2

RADIO_CFG = [Radio(0, 915, -119, 0), Radio(1, 2400, -89,0)]
D_0             = 1     # d_nought parameter (reference distance)
PL_SIGMA        = 2  # Shadowing component of path loss

### Action Space Constants
USE_RADIO_0 = 0
USE_RADIO_1 = 1
#NOP         = 2     ## keep using same radio as prev


PATH_POINTS = []

def PLE_for_point(PLE, polygons, point):
    for p in polygons:
        if p.contains(point):
            return PLE[polygons.index(p)]


class PathWirelessEnv(gym.Env):
    metadata = {'render.modes' : ['human']}


    def __init__(self, update_interval, god_agent=False):
        print('init path wireless env')
        self.src_node = Node(0,0,RADIO_CFG)
        self.dst_node = Node(int(DST_NODE_X),int(DST_NODE_Y),RADIO_CFG)

        self.action_space = spaces.Discrete(2)

        self.grid_render = None

        if (god_agent == True):
            self.god_agent = True
        else:
            self.god_agent = False

        
        self.time_int = 1 #time interval in seconds
        self.time_int_steps = self.time_int/update_interval
        
        self.steps_since_int = 0

        self.env_polygons = []
        self.env_PLE = []
        self.path_points = []
        self.path_count = 0
        self.path_len = 0

    def step(self, action):
        src_node_x = self.src_node.get_x_pos()
        src_node_y = self.src_node.get_y_pos()
        
        if (self.src_node.get_curr_radio_id() != action):
            radio_changed = 1
        else:
            radio_changed = 0

        if (action == USE_RADIO_0):
            self.src_node.set_curr_radio_id(0)
        elif (action == USE_RADIO_1):
            self.src_node.set_curr_radio_id(1)
        else:
            pass

        
        PLE = PLE_for_point(self.env_PLE, self.env_polygons, Point(src_node_x, src_node_y))
        #PLE = 2
        dist = distance.euclidean(self.src_node.get(), self.dst_node.get())
        #shadowing = np.random.normal(0, PL_SIGMA)
        shadowing = 0
        for radio in self.src_node.get_radios():
            
            lm = const.speed_of_light/(radio.get_freq()*np.power(10,6))
            K = 20*np.log10(lm/(4*const.pi*D_0))
            
            P_r = int(radio.get_tx_power() + K - (10 * PLE * np.log10(dist/D_0)) - shadowing)
            fade_margin = P_r - radio.get_rx_sensitivity()
            self.src_node.update_radio_dst_stats(radio.get_radio_id(), P_r, fade_margin)
            #sys.stdout.write("radio: " + str(radio.get_freq()) + "\tP_r=" + str(P_r) + "dBm\tMargin=" + str(fade_margin) + "\n")


        curr_radio = self.src_node.get_radios()[self.src_node.get_curr_radio_id()]

        if (self.god_agent == True):
            obs = self.src_node.get_radios()
        else:
            obs = curr_radio
        
        fade_margin = [0,0]

        for i in range(self.src_node.get_radios().__len__()):
            fade_margin[i] = self.src_node.get_radios()[i].get_fade_margin_to_dst()
        
        #print(fade_margin)
        #print(self.src_node.get_curr_radio_id())

        reward = 0

        if (radio_changed):
            if (curr_radio.get_radio_id() == 0):
                if (fade_margin[0] > 0 and fade_margin[1] > 0):
                    reward = -5
                    #print("reward 1")
                elif (fade_margin[0] > 0 and fade_margin[1] <= 0):
                    reward = 2
                    #print("reward 2")
                else:
                    reward = 0
            elif (curr_radio.get_radio_id() == 1):
                if (fade_margin[0] <= 0 and fade_margin[1] > 0):
                    reward = 2
                    #print("reward 3")
                elif (fade_margin[0] > 0 and fade_margin[1] <= 0):
                    reward = -5
                    #print("reward 4")
                elif (fade_margin[0] > 0 and fade_margin[1] > 0):
                    reward = 2
                    #print("reward 5")
                else:
                    reward = 0
        else:
            if (curr_radio.get_radio_id() == 0):
                if (fade_margin[0] > 0 and fade_margin[1] > 0):
                    reward = -5
                    #print("reward 6")
                elif (fade_margin[0] > 0 and fade_margin[1] <= 0):
                    reward = 2
                    #print("reward 7")
            elif (curr_radio.get_radio_id() == 1):
                if (fade_margin[0] <= 0 and fade_margin[1] > 0):
                    reward = 2
                    #print("reward 8")
                elif (fade_margin[0] > 0 and fade_margin[1] > 0):
                    reward = 2
                    #print("reward 9")
                elif (fade_margin[0] > 0 and fade_margin[1] <= 0):
                    reward = -5
                    #print("reward 10")

        self.path_count += 1
        
        
        if (self.path_count >= self.path_len):
            done = True
        else:
            self.src_node.set_x_pos(self.path_points[self.path_count][0])
            self.src_node.set_y_pos(self.path_points[self.path_count][1])
            done = False
        
        return obs, reward, done, {}

        


    def reset(self):
        self.path_points = []
        self.path_len = 0
        self.env_polygons = []
        self.env_PLE = []

        print('loading path')
        with open(FILE_DIR+'a_1.csv', 'r', newline='') as file:
            lines = csv.reader(file, delimiter=',')
            for line in lines:
                #print([int(line[0]), int(line[1])])
                self.path_points.append([int(line[0]), int(line[1])])
        
        
        print('loading polygons')
        

        with open(FILE_DIR+'polys.csv', 'r') as file:
            lines = csv.reader(file, delimiter=',')
            env = next(lines)
            
            line=next(lines)
            while line:
            #for line in lines:
                p = []
                
                for i in range(1, len(line), 2):
                    p.append([float(line[i]), float(line[i+1])])
                poly  = Polygon(p)
                
                self.env_polygons.append(poly)
                self.env_PLE.append(float(line[0]))

                line = next(lines, None)
            

            norm = mpl.colors.Normalize(vmin=1.5, vmax=6.0, clip=False)
            mapper = cm.ScalarMappable(norm=norm, cmap=cm.viridis)
            for p in self.env_polygons:
                plt.fill(*p.exterior.xy, color=mapper.to_rgba(self.env_PLE[self.env_polygons.index(p)]))

            plt.xlim(0, 1000)
            plt.ylim(0, 1000)
            plt.axis([0,1000,0,1000])
            plt.show()


        self.src_node.set_x_pos(self.path_points[0][0])
        self.src_node.set_y_pos(self.path_points[0][1])

        self.dst_node.set_x_pos(SPACE_W/2)
        self.dst_node.set_y_pos(SPACE_H/2)

        self.path_len = len(self.path_points)
        self.path_count += 1

        print('total path length: ' + str(self.path_len) + ' steps')
    
    def render(self, mode='human'):
        return
    

    def close(self):
        pass