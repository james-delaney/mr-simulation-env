import gym
from gym import error, spaces, utils
from gym.utils import seeding
from gym_grid_wireless.node import Node
from gym_grid_wireless.radio import Radio
import numpy as np
import sys
import os
import csv
from scipy.spatial import distance
from scipy import constants as const
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import gridspec
from gym_grid_wireless.multi_radio_actions import Actions

FILE_DIR = os.path.dirname(__file__) + '/'

SPACE_W         = 1000
SPACE_H         = 1000

DST_NODE_X      = 0 #SPACE_W/2
DST_NODE_Y      = 0 #SPACE_H/2

#RADIO_CFG = [   Radio(0, 915, -119, [7,15,18,21,24], [4,27,54,107,215]), 
#                Radio(1, 2400, -89, [1,5,11,15,19.5], [1.8,7.8,31,83,240])]
D_0             = 1     # d_nought parameter (reference distance) = 10m
PL_SIGMA        = 3  # Shadowing component of path loss

### Action Space Constants
USE_RADIO_0 = 0
USE_RADIO_1 = 1
NOP         = 2     ## no tx, stay quiet


PATH_POINTS = []

def PLE_for_point(PLE, polygons, point):
    for p in polygons:
        if p.contains(point):
            return PLE[polygons.index(p)]
    
    return 3.5

class MeasWirelessEnv(gym.Env):
    metadata = {'render.modes' : ['human']}


    def __init__(self, update_interval, radio_cfg, env_fname, god_agent=False):
        print('init path wireless env')
        #self.src_node = Node(0,0,RADIO_CFG)
        #self.dst_node = Node(int(DST_NODE_X),int(DST_NODE_Y),RADIO_CFG)

        self.action_space = spaces.Discrete(len(Actions))

        self.grid_render = None

        if (god_agent == True):
            self.god_agent = True
        else:
            self.god_agent = False

        
        self.time_int = 1 #time interval in seconds
        self.time_int_steps = self.time_int/update_interval
        
        self.steps_since_int = 0

        self.env_polygons = []
        self.env_PLE = []
        self.path_points = []
        self.path_count = 0
        self.path_len = 0

        self.last_PLE = 0
        self.last_dist = 0
        self.last_shadowing = 0

        self.total_steps = 0
        self.last_action = 0



        self.env_PLE = []
        self.PLE_points = []
        self.path_points_x = []
        self.path_points_y = []

        self.rssi_points = [[],[]]

        self.radio_cfg = radio_cfg
        #self.agent_path_fname = agent_path_fname
        self.env_fname = env_fname
        #self.ple_fname = ple_fname

        print('loading path')
        with open(FILE_DIR+self.env_fname, 'r', newline='') as file:
            lines = csv.reader(file, delimiter=',')
            for line in lines:
                #print([int(line[0]), int(line[1])])
                self.path_points.append([float(line[0]), float(line[1])])
                self.path_points_x.append(float(line[0]))
                self.path_points_y.append(float(line[1]))
                self.rssi_points[0].append(int(float(line[2])))
                self.rssi_points[1].append(int(float(line[3])))
        
            
            # Comment out plot of environment and path
            '''
            fig = plt.figure(1)
            gs = gridspec.GridSpec(16, 10)

            ax = plt.subplot2grid((16,20), (0,17), colspan=1, rowspan=16)
            ax2 = plt.subplot2grid((16,20), (0,0), colspan=16, rowspan=16)
        
            norm = mpl.colors.Normalize(vmin=2, vmax=6.0, clip=False)
            mapper = cm.ScalarMappable(norm=norm, cmap=cm.viridis)
            for p in self.env_polygons:
                ax2.fill(*p.exterior.xy, edgecolor="black",facecolor=mapper.to_rgba(self.env_PLE[self.env_polygons.index(p)]))


            cb1 = mpl.colorbar.ColorbarBase(ax, cmap=cm.viridis, norm=norm, orientation='vertical')
            cb1.set_label('Path Loss Exponent')
            mpl.rcParams.update({'font.size': 10})


            ax2.plot(self.path_points_x, self.path_points_y, 'r', label="Mobile node path")
            ax2.plot(DST_NODE_X,DST_NODE_Y, 'oy', markersize=5, label="Stationary node")
            ax2.set_xticks([])
            ax2.set_yticks([])
            ax2.legend(prop={'size':8})
            plt.xlim(0, 1000)
            plt.ylim(0, 1000)
            #plt.axis([0,1000,0,1000])
            plt.title('Mobile Node Trajectory')
            plt.show()
            '''
            ##
        '''
        for i in range(0, len(self.path_points)):
            if (i % 1000) == 0:
                print("path loaded: " + str(i))
            self.PLE_points.append(PLE_for_point(self.env_PLE, self.env_polygons, Point(self.path_points[i][0], self.path_points[i][1])))
        '''



    def calc_reward(self, curr_radio, radio_changed, action, alt_radio, last_action):
        reward = 0

        # This reward structure rewards choosing the higher efficiency bit rate (ie. more bits/mA) 
        #  and by extension lower transmit powers
        if (action == Actions.NO_TX):
            if (not curr_radio.get_link_status()) and  (not alt_radio.get_link_status()):
                reward = 1
            elif (curr_radio.get_link_status() or alt_radio.get_link_status()):
                
                reward = -1
                return reward
        elif (curr_radio.get_link_status()):
                #reward += 1
                #reward -= (1 - self.norm_kbit_per_ma[curr_radio.get_radio_id()][curr_radio.get_tx_pow_step()])
                #print('curr radio ' + str(curr_radio.get_radio_id()))

                
                if (alt_radio.get_link_status()):
                    if (alt_radio.get_bit_rate() > curr_radio.get_bit_rate()):
                        reward -= 1
                    else:
                        reward += (curr_radio.get_bit_rate())/1000  * self.norm_kbit_per_ma[curr_radio.get_radio_id()][curr_radio.get_tx_pow_step()]   
                        #reward += 1
                else:
                    reward += (curr_radio.get_bit_rate())/1000 * self.norm_kbit_per_ma[curr_radio.get_radio_id()][curr_radio.get_tx_pow_step()] 
                    #reward += 1
                
                
                #reward += (curr_radio.get_bit_rate())/1000
                ## No weighting
                #reward =  1 * self.norm_kbit_per_ma[curr_radio.get_radio_id()][curr_radio.get_tx_pow_step()]
                
                ## Weighted by bit rate - higher bit rate preferred
                #if (curr_radio.get_freq() == 2400):
                #    print('eff ' + str(self.norm_kbit_per_ma[curr_radio.get_radio_id()][curr_radio.get_tx_pow_step()]))
                #reward += ((curr_radio.get_bit_rate())/1000) * (self.norm_kbit_per_ma[curr_radio.get_radio_id()][curr_radio.get_tx_pow_step()])

                ## Inverse weighting by bit rate - lower rate preferred to save power
                #reward += (1 - ((curr_radio.get_bit_rate())/1000)) * (self.norm_kbit_per_ma[curr_radio.get_radio_id()][curr_radio.get_tx_pow_step()])

                ## Scaled bitrate weighting
                '''
                if (curr_radio.get_bit_rate() == 1000):
                    reward += 6 * (self.norm_kbit_per_ma[curr_radio.get_radio_id()][curr_radio.get_tx_pow_step()])
                else:
                    reward += ((curr_radio.get_bit_rate())/1000) * (self.norm_kbit_per_ma[curr_radio.get_radio_id()][curr_radio.get_tx_pow_step()])
                '''
        else:
            #print(self.total_steps, action)
            reward = -1 #* self.norm_kbit_per_ma[curr_radio.get_radio_id()][curr_radio.get_tx_pow_step()]
        
        
        
        
        '''
        if (curr_radio.get_link_status()):
            if (curr_radio.get_radio_id() == 0):
                if (alt_radio.get_link_status()):
                    return -1
                else:
                    return 1
            else:
                return 1
        else:
            if (alt_radio.get_link_status()):
                return -1
            else:
                if (action == Actions.NO_TX):
                    return 1
                else:
                    return -1
        '''

        '''
        if (radio_changed):
            #reward -= 0.2     # cost for changing radio
            if (curr_radio.get_link_status()):
                #reward += 1
                #reward -= (1 - self.norm_kbit_per_ma[curr_radio.get_radio_id()][curr_radio.get_tx_pow_step()])
                reward =  1 * self.norm_kbit_per_ma[curr_radio.get_radio_id()][curr_radio.get_tx_pow_step()]
                
                #reward += (curr_radio.get_bit_rate())/1000
                #reward += ((curr_radio.get_bit_rate())/1000) * (self.norm_kbit_per_ma[curr_radio.get_radio_id()][curr_radio.get_tx_pow_step()])
            else:
                reward = -1 * self.norm_kbit_per_ma[curr_radio.get_radio_id()][curr_radio.get_tx_pow_step()]
        else:
            if (curr_radio.get_link_status()):
                #reward += 1
                #reward -= (1 - self.norm_kbit_per_ma[curr_radio.get_radio_id()][curr_radio.get_tx_pow_step()])
                reward  =  1 * self.norm_kbit_per_ma[curr_radio.get_radio_id()][curr_radio.get_tx_pow_step()]
                #reward += (curr_radio.get_bit_rate())/1000
                #reward += ((curr_radio.get_bit_rate())/1000) * (self.norm_kbit_per_ma[curr_radio.get_radio_id()][curr_radio.get_tx_pow_step()])
            else:
                reward = -1 * self.norm_kbit_per_ma[curr_radio.get_radio_id()][curr_radio.get_tx_pow_step()]
        '''

        return reward

    def step(self, action):
        src_node_x = self.src_node.get_x_pos()
        src_node_y = self.src_node.get_y_pos()
        
        if (action >= Actions.RADIO_0_TX_POW_0 and action <= Actions.RADIO_0_TX_POW_4):
            radio = USE_RADIO_0
        elif (action >= Actions.RADIO_1_TX_POW_0 and action <= Actions.RADIO_1_TX_POW_4):
            radio = USE_RADIO_1
        elif (action == Actions.NO_TX):
            radio = NOP


        if (self.src_node.get_curr_radio_id() != radio):
            radio_changed = 1
        else:
            radio_changed = 0

        if (radio == USE_RADIO_0):
            self.src_node.set_curr_radio_id_and_tx_pow(0, action)
        elif (radio == USE_RADIO_1):
            self.src_node.set_curr_radio_id_and_tx_pow(1, action-5) # offset for radio 1 (this is a bit of a hack)
        else:
            pass

        
        #PLE = PLE_for_point(self.env_PLE, self.env_polygons, Point(src_node_x, src_node_y))
        PLE = 3.2
        #PLE = self.PLE_points[self.path_count]
        #print(Point(src_node_x, src_node_y))
        #if PLE != self.last_PLE:
        #  print("PLE changed: " + str(self.total_steps) + ", " + str(PLE))
        
        dist = distance.euclidean(self.src_node.get(), self.dst_node.get())+1
        #print(dist)
        '''
        if dist != self.last_dist:
            shadowing = np.random.normal(0, PL_SIGMA)
            #print(shadowing)
        else :
            shadowing = self.last_shadowing
        '''
        #shadowing = 0
        for radio in self.src_node.get_radios():
            '''
            lm = const.speed_of_light/(radio.get_freq()*np.power(10,6))
            K = 20*np.log10(lm/(4*const.pi*D_0))
            
            P_r = int(radio.get_tx_pow() + K - (10 * PLE * np.log10(dist/D_0)) - shadowing)
            '''
            P_r = self.rssi_points[radio.get_radio_id()][self.total_steps]
            fade_margin = P_r - radio.get_rx_sensitivity()
            

            self.src_node.update_radio_dst_stats(radio.get_radio_id(), P_r, fade_margin)
            
            #sys.stdout.write("radio: " + str(radio.get_freq()) + "\tP_r=" + str(P_r) + "dBm\tMargin=" + str(fade_margin) + "\n")
            
        #self.last_PLE = PLE
        #self.last_shadowing = shadowing
        #self.last_dist = dist




        #if action == NOP:
        #    curr_radio = None
        #else:
        curr_radio = self.src_node.get_radios()[self.src_node.get_curr_radio_id()]
        alt_radio = self.src_node.get_radios()[self.src_node.get_curr_radio_id()-1]
        #print ('curr ' + str(curr_radio.get_radio_id()))
        #print(action)
        if (self.god_agent == True):
            obs = self.src_node.get_radios()
        else:
            obs = curr_radio
        
        fade_margin = [0,0]
        for i in range(self.src_node.get_radios().__len__()):
            fade_margin[i] = self.src_node.get_radios()[i].get_fade_margin_to_dst()
        
        #print(fade_margin)
        #print(self.src_node.get_curr_radio_id())

        reward = self.calc_reward(curr_radio, radio_changed, action, alt_radio, self.last_action)

       
        self.path_count += 1
        
        
        if (self.path_count >= self.path_len):
            done = True
        else:
            self.src_node.set_x_pos(self.path_points[self.path_count][0])
            self.src_node.set_y_pos(self.path_points[self.path_count][1])
            done = False
        
        self.total_steps += 1
        self.last_action = action

        return obs, reward, done, {'dist':dist}

        


    def reset(self):
        print('resetting environment')
        self.src_node = None
        self.dst_node = None
        self.src_node = Node(0, 0, self.radio_cfg)
        self.dst_node = Node(int(DST_NODE_X), int(DST_NODE_Y), self.radio_cfg)

        num_radios = len(self.radio_cfg)
        self.norm_kbit_per_ma = []

        max_val = 0
        min_val = 10000
        a = self.radio_cfg[0].get_bits_per_ma()
        b = self.radio_cfg[1].get_bits_per_ma()
        #print(a, b)
        for i in range(0,5):
            if (a[i] > max_val):
                max_val = a[i]
            if (b[i] > max_val):
                max_val = b[i]

            if (a[i] < min_val):
                min_val = a[i]
            if (b[i] < min_val):
                min_val = b[i]

        #print(min_val, max_val)
        self.norm_kbit_per_ma.append(a.copy())
        self.norm_kbit_per_ma.append(b.copy())
        
        min_val = 0
        #print(self.norm_kbit_per_ma)
        for x in range(0,5):
            self.norm_kbit_per_ma[0][x] = (self.norm_kbit_per_ma[0][x] - min_val)/(max_val-min_val)
            self.norm_kbit_per_ma[1][x] = (self.norm_kbit_per_ma[1][x] - min_val)/(max_val-min_val)

        #print(self.norm_kbit_per_ma)
        self.path_count = 0

        self.last_PLE = 0
        self.total_steps = 0
        self.last_action = 0

        self.src_node.set_x_pos(self.path_points[0][0])
        self.src_node.set_y_pos(self.path_points[0][1])

        self.dst_node.set_x_pos(DST_NODE_X)
        self.dst_node.set_y_pos(DST_NODE_Y)

        self.path_len = len(self.path_points)
        self.path_count += 1

        print('total path length: ' + str(self.path_len) + ' steps')
    
    def render(self, mode='human'):
        return
    

    def close(self):
        pass