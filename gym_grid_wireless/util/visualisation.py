

def plt_env_path(radio_use_trace, outage_trace):
    env_polygons = []
    env_PLE = []
    path_points = []
    path_count = 0
    path_len = 0
    env_PLE = []
    PLE_points = []
    path_points_x = []
    path_points_y = []

    print('loading path')
    with open(FILE_DIR+PATH_FNAME, 'r', newline='') as file:
        lines = csv.reader(file, delimiter=',')
        for line in lines:
            #print([int(line[0]), int(line[1])])
            path_points.append([int(line[0]), int(line[1])])
            path_points_x.append(int(line[0]))
            path_points_y.append(int(line[1]))
    
    
    print('loading polygons')
    with open(FILE_DIR+ENV_FNAME, 'r') as file:
        lines = csv.reader(file, delimiter=',')
        env = next(lines)
        
        line=next(lines)
        while line:
        #for line in lines:
            p = []
            
            for i in range(1, len(line), 2):
                p.append([float(line[i]), float(line[i+1])])
            poly  = Polygon(p)
            
            env_polygons.append(poly)
            env_PLE.append(float(line[0]))

            line = next(lines, None)
        
        # Comment out plot of environment and path
        
        fig = plt.figure(1)
        fig.suptitle('Mobile Node Trajectory and Adaptive Radio Selection')
        gs = gridspec.GridSpec(16, 10)

        ax = plt.subplot2grid((16,20), (0,17), colspan=1, rowspan=16)
        ax2 = plt.subplot2grid((16,20), (0,0), colspan=16, rowspan=16)
    
        norm = mpl.colors.Normalize(vmin=2.0, vmax=6.0, clip=False)
        mapper = cm.ScalarMappable(norm=norm, cmap=cm.viridis)
        for p in env_polygons:
            ax2.fill(*p.exterior.xy, edgecolor="black",facecolor=mapper.to_rgba(env_PLE[env_polygons.index(p)]))


        cb1 = mpl.colorbar.ColorbarBase(ax, cmap=cm.viridis, norm=norm, orientation='vertical')
        cb1.set_label('Path Loss Exponent')
        mpl.rcParams.update({'font.size': 10})



        #ax2.plot(path_points_x, path_points_y, 'r', label="Mobile node path")
        
        r0_points_x = []
        r0_points_y = []
        r1_points_x = []
        r1_points_y = []
        nr_points_x = []
        nr_points_y = []

        r0_dn_points_x = []
        r0_dn_points_y = []
        r1_dn_points_x = []
        r1_dn_points_y = []
        
        for i in range(0, PATH_LEN):
            if i % 1000 == 0:
                print("count: " + str(i)) 
            if radio_use_trace[i] == 0:
                #print("num " + str(i) + "r0")
                r0_points_x.append(path_points_x[i])
                r0_points_y.append(path_points_y[i])
                #ax2.scatter(int(path_points_x[i]), int(path_points_y[i]), c='y')
            elif radio_use_trace[i] == 1:
                #print("num " + str(i) + "r1")
                r1_points_x.append(path_points_x[i])
                r1_points_y.append(path_points_y[i])
                #ax2.scatter(int(path_points_x[i]), int(path_points_y[i]), c='c')
            else:
                #print("num " + str(i) + "nr")
                nr_points_x.append(path_points_x[i])
                nr_points_y.append(path_points_y[i])
                #ax2.scatter(int(path_points_x[i]), int(path_points_y[i]), c='r')

            if outage_trace[i] == 1:
                r0_dn_points_x.append(path_points_x[i])
                r0_dn_points_y.append(path_points_y[i])
        
        print(len(r0_dn_points_x), len(r1_dn_points_x))
        #print(radio_use_trace)

        ax2.plot(r0_points_x, r0_points_y, 'om', markersize=1, c='m', label='915MHz Radio')
        ax2.plot(r1_points_x, r1_points_y, 'oc', markersize=1, c='c', label='2.4GHz Radio')
        ax2.plot(nr_points_x, nr_points_y, 'or', markersize=1, c='r', label='Radio off')
        ax2.plot(r0_dn_points_x, r0_dn_points_y, 'ob', markersize=1, c='b', label='915MHz Radio down')
        ax2.plot(r1_dn_points_x, r1_dn_points_y, 'og', markersize=1, c='g', label='2.4GHz Radio down')

        ax2.plot(DST_NODE_X,DST_NODE_Y, 'oy', markersize=5, label="Stationary node")
        #ax2.set_xticks([])
        #ax2.set_yticks([])
        ax2.legend(prop={'size':8}, loc=4)
        plt.xlim(0, 500)
        plt.ylim(0, 500)
        
        plt.axis([0,500,0,500])