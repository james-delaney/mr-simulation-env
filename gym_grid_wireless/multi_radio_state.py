import enum
from gym_grid_wireless.node import Node
from gym_grid_wireless.radio import Radio
from gym_grid_wireless.multi_radio_actions import Actions, LowPowerActions, HighPowerActions

class States(enum.IntEnum):
    RADIO_0_LINK_UP_FM_INC_0    = 0
    RADIO_0_LINK_UP_FM_DEC_0    = 1
    RADIO_0_LINK_UP_FM_STB_0    = 2 
    RADIO_0_LINK_UP_FM_INC_1    = 3
    RADIO_0_LINK_UP_FM_DEC_1    = 4
    RADIO_0_LINK_UP_FM_STB_1    = 5
    RADIO_0_LINK_UP_FM_INC_2    = 6
    RADIO_0_LINK_UP_FM_DEC_2    = 7
    RADIO_0_LINK_UP_FM_STB_2    = 8
    RADIO_0_LINK_DN             = 9
    RADIO_1_LINK_UP_FM_INC_0    = 10
    RADIO_1_LINK_UP_FM_DEC_0    = 11
    RADIO_1_LINK_UP_FM_STB_0    = 12
    RADIO_1_LINK_UP_FM_INC_1    = 13
    RADIO_1_LINK_UP_FM_DEC_1    = 14
    RADIO_1_LINK_UP_FM_STB_1    = 15
    RADIO_1_LINK_UP_FM_INC_2    = 16
    RADIO_1_LINK_UP_FM_DEC_2    = 17
    RADIO_1_LINK_UP_FM_STB_2    = 18
    RADIO_1_LINK_DN             = 19
    RADIO_OFF                   = 20
    RADIO_0_LINK_UP_FM_STB_3    = 21
    RADIO_0_LINK_UP_FM_STB_4    = 22
    RADIO_0_LINK_UP_FM_STB_5    = 23
    RADIO_0_LINK_UP_FM_STB_6    = 24
    RADIO_0_LINK_UP_FM_STB_7    = 25
    RADIO_1_LINK_UP_FM_STB_3    = 26
    RADIO_1_LINK_UP_FM_STB_4    = 27
    RADIO_1_LINK_UP_FM_STB_5    = 28
    RADIO_1_LINK_UP_FM_STB_6    = 29
    RADIO_1_LINK_UP_FM_STB_7    = 30

class PowerStates(enum.IntEnum):
    RADIO_0_0                   = 0
    RADIO_0_1                   = 1
    RADIO_0_2                   = 2
    RADIO_0_3                   = 3
    RADIO_0_4                   = 4
    RADIO_1_0                   = 5
    RADIO_1_1                   = 6
    RADIO_1_2                   = 7
    RADIO_1_3                   = 8
    RADIO_1_4                   = 9
    RADIO_OFF                   = 10

class ExtPowerStates(enum.IntEnum):
    RADIO_0_0                   = 0
    RADIO_0_1                   = 1
    RADIO_0_2                   = 2
    RADIO_0_3                   = 3
    RADIO_0_4                   = 4
    RADIO_1_0                   = 5
    RADIO_1_1                   = 6
    RADIO_1_2                   = 7
    RADIO_1_3                   = 8
    RADIO_1_4                   = 9
    RADIO_OFF                   = 10
    RADIO_0_0_DN                = 11
    RADIO_0_1_DN                = 12
    RADIO_0_2_DN                = 13
    RADIO_0_3_DN                = 14
    RADIO_0_4_DN                = 15
    RADIO_1_0_DN                = 16
    RADIO_1_1_DN                = 17
    RADIO_1_2_DN                = 18
    RADIO_1_3_DN                = 19
    RADIO_1_4_DN                = 20

class RadioPowerStates(enum.IntEnum):
    RADIO_0_TX_POW_0        = 0
    RADIO_0_TX_POW_1        = 1
    RADIO_0_TX_POW_2        = 2
    RADIO_0_TX_POW_3        = 3
    RADIO_0_TX_POW_4        = 4
    RADIO_1_TX_POW_0        = 5
    RADIO_1_TX_POW_1        = 6
    RADIO_1_TX_POW_2        = 7
    RADIO_1_TX_POW_3        = 8
    RADIO_1_TX_POW_4        = 9
    NO_TX                   = 10
    RADIO_0_LINK_DN         = 11
    RADIO_1_LINK_DN         = 12


class RadioSNRStates(enum.IntEnum):
    RADIO_0_LINK_DN         = 0
    RADIO_0_LOW             = 1
    RADIO_0_LOW_MED         = 2
    RADIO_0_MED             = 3
    RADIO_0_MED_HIGH        = 4
    RADIO_0_HIGH            = 5
    RADIO_1_LINK_DN         = 6
    RADIO_1_LOW             = 7
    RADIO_1_LOW_MED         = 8
    RADIO_1_MED             = 9
    RADIO_1_MED_HIGH        = 10
    RADIO_1_HIGH            = 11

class MultiRadioState():



    def __init__(self, radios, fm_limit_0=5, fm_limit_1=10, fm_limit_2=25, fm_limit_3=30, fm_limit_4=35, fm_limit_5=40, fm_limit_6=45):
        self.radios = radios

        self.fm_limit_0 = fm_limit_0
        self.fm_limit_1 = fm_limit_1
        self.fm_limit_2 = fm_limit_2
        self.fm_limit_3 = fm_limit_3
        self.fm_limit_4 = fm_limit_4
        self.fm_limit_5 = fm_limit_5
        self.fm_limit_6 = fm_limit_6
        
        self.last_radio = 0
        self.last_fm = 0

    def update_obs(self, radio_id, radio):
        self.radios[radio_id] = radio

    def get_rssi_state(self, curr_radio_id, action):
        radio = self.radios[curr_radio_id]
        fm = radio.get_fade_margin_to_dst()

        state = (curr_radio_id*200) + fm

        if (action == Actions.NO_TX):
            state = 400 # radio off state when encoding rssi as state

        return state

    def get_state(self, curr_radio_id, action):
        radio = self.radios[curr_radio_id]
        fm = radio.get_fade_margin_to_dst()
        #if (curr_radio_id == 1):
         #   print(fm, self.last_fm)

        radio_changed = (self.last_radio != curr_radio_id)
        '''
        if not radio_changed:
            fm = radio.get_fade_margin_to_dst()
        else:
            fm = 0
        '''
        if (action == Actions.NO_TX):
            #state = States.RADIO_OFF
            state = RadioPowerStates.NO_TX
            return state
        '''
        if (fm > 0):
            if (fm < self.fm_limit_0):
                if (fm - self.last_fm ) > 2:
                    if curr_radio_id == 0:
                        state = States.RADIO_0_LINK_UP_FM_INC_0
                    else:
                        state = States.RADIO_1_LINK_UP_FM_INC_0
                elif (fm - self.last_fm) < -2:
                    if curr_radio_id == 0:
                        state = States.RADIO_0_LINK_UP_FM_DEC_0
                    else:
                        state = States.RADIO_1_LINK_UP_FM_DEC_0
                else:
                    if curr_radio_id == 0:
                        state = States.RADIO_0_LINK_UP_FM_STB_0
                    else:
                        state = States.RADIO_1_LINK_UP_FM_STB_0
            elif (fm >= self.fm_limit_0) and (fm < self.fm_limit_1):
                if (fm - self.last_fm ) > 2:
                    if curr_radio_id == 0:
                        state = States.RADIO_0_LINK_UP_FM_INC_1
                    else:
                        state = States.RADIO_1_LINK_UP_FM_INC_1
                elif (fm - self.last_fm) < -2:
                    if curr_radio_id == 0:
                        state = States.RADIO_0_LINK_UP_FM_DEC_1
                    else:
                        state = States.RADIO_1_LINK_UP_FM_DEC_1
                else:
                    if curr_radio_id == 0:
                        state = States.RADIO_0_LINK_UP_FM_STB_1
                    else:
                        state = States.RADIO_1_LINK_UP_FM_STB_1
            elif (fm >= self.fm_limit_1):
                if (fm - self.last_fm ) > 2:   
                    if curr_radio_id == 0:
                        state = States.RADIO_0_LINK_UP_FM_INC_2
                    else:
                        state = States.RADIO_1_LINK_UP_FM_INC_2
                elif (fm - self.last_fm) < -2:
                    if curr_radio_id == 0:
                        state = States.RADIO_0_LINK_UP_FM_DEC_2
                    else:
                        state = States.RADIO_1_LINK_UP_FM_DEC_2
                else:
                    if curr_radio_id == 0:
                        state = States.RADIO_0_LINK_UP_FM_STB_2
                    else:
                        state = States.RADIO_1_LINK_UP_FM_STB_2
            else:
                print(fm)
        else:
            if (curr_radio_id == 0):
                state = States.RADIO_0_LINK_DN
            else:
                state = States.RADIO_1_LINK_DN
        
        '''
        '''
        #if (fm > 0) and (radio.get_link_status()):
        if (radio.get_link_status()):
            if (fm < self.fm_limit_0):
                if curr_radio_id == 0:
                    state = States.RADIO_0_LINK_UP_FM_STB_0
                else:
                    state = States.RADIO_1_LINK_UP_FM_STB_0
            elif (fm >= self.fm_limit_0) and (fm < self.fm_limit_1):
                if curr_radio_id == 0:
                    state = States.RADIO_0_LINK_UP_FM_STB_1
                else:
                    state = States.RADIO_1_LINK_UP_FM_STB_1
            elif (fm >= self.fm_limit_1):
                if curr_radio_id == 0:
                    state = States.RADIO_0_LINK_UP_FM_STB_2
                else:
                    state = States.RADIO_1_LINK_UP_FM_STB_2
        else:
            if (curr_radio_id == 0):
                state = States.RADIO_0_LINK_DN
            else:
                state = States.RADIO_1_LINK_DN
        '''

        
        if (radio.get_link_status()):
            if (fm < self.fm_limit_0):
                if curr_radio_id == 0:
                    state = States.RADIO_0_LINK_UP_FM_STB_0
                else:
                    state = States.RADIO_1_LINK_UP_FM_STB_0
            elif (fm >= self.fm_limit_0) and (fm < self.fm_limit_1):
                if curr_radio_id == 0:
                    state = States.RADIO_0_LINK_UP_FM_STB_1
                else:
                    state = States.RADIO_1_LINK_UP_FM_STB_1
            elif (fm >= self.fm_limit_1) and (fm < self.fm_limit_2):
                if curr_radio_id == 0:
                    state = States.RADIO_0_LINK_UP_FM_STB_2
                else:
                    state = States.RADIO_1_LINK_UP_FM_STB_2
            elif (fm >= self.fm_limit_2) and (fm < self.fm_limit_3):
                if curr_radio_id == 0:
                    state = States.RADIO_0_LINK_UP_FM_STB_3
                else:
                    state = States.RADIO_1_LINK_UP_FM_STB_3
            elif (fm >= self.fm_limit_3) and (fm < self.fm_limit_4):
                if curr_radio_id == 0:
                    state = States.RADIO_0_LINK_UP_FM_STB_4
                else:
                    state = States.RADIO_1_LINK_UP_FM_STB_4
            elif (fm >= self.fm_limit_4) and (fm < self.fm_limit_5):
                if curr_radio_id == 0:
                    state = States.RADIO_0_LINK_UP_FM_STB_5
                else:
                    state = States.RADIO_1_LINK_UP_FM_STB_5
            elif (fm >= self.fm_limit_5) and (fm < self.fm_limit_6):
                if curr_radio_id == 0:
                    state = States.RADIO_0_LINK_UP_FM_STB_6
                else:
                    state = States.RADIO_1_LINK_UP_FM_STB_6
            elif (fm >= self.fm_limit_6):
                if curr_radio_id == 0:
                    state = States.RADIO_0_LINK_UP_FM_STB_7
                else:
                    state = States.RADIO_1_LINK_UP_FM_STB_7
        else:
            if (curr_radio_id == 0):
                state = States.RADIO_0_LINK_DN
            else:
                state = States.RADIO_1_LINK_DN

        '''
        if (not radio.get_link_status()):
            if curr_radio_id == 0:
                state = RadioPowerStates.RADIO_0_LINK_DN
            else:
                state = RadioPowerStates.RADIO_1_LINK_DN
        else:
            state = action
        '''
        self.last_fm = fm
        return state
    
class MultiRadioPowerState():

    def __init__(self, radios):
        self.radios = radios

    def update_obs(self, radio_id, radio):
        self.radios[radio_id] = radio

    def get_state(self, curr_radio_id, action):
        state = 0
        curr_tx_pow = self.radios[curr_radio_id].get_tx_pow_step()

        
        if action == Actions.NO_TX:
            return PowerStates.RADIO_OFF

        if (curr_radio_id == 0):
            if (curr_tx_pow == 0):
                state = PowerStates.RADIO_0_0
            elif (curr_tx_pow == 1):
                state = PowerStates.RADIO_0_1
            elif (curr_tx_pow == 2):
                state = PowerStates.RADIO_0_2
            elif (curr_tx_pow == 2):
                state = PowerStates.RADIO_0_3
            elif (curr_tx_pow == 3):
                state = PowerStates.RADIO_0_4
        elif (curr_radio_id == 1):
            if (curr_tx_pow == 0):
                state = PowerStates.RADIO_1_0
            elif (curr_tx_pow == 1):
                state = PowerStates.RADIO_1_1
            elif (curr_tx_pow == 2):
                state = PowerStates.RADIO_1_2
            elif (curr_tx_pow == 2):
                state = PowerStates.RADIO_1_3
            elif (curr_tx_pow == 3):
                state = PowerStates.RADIO_1_4
        else:
            return 0
        
        return state

class MultiRadioExtPowerState():
    
    def __init__(self, radios):
        self.radios = radios
        
    def update_obs(self, radio_id, radio):
        self.radios[radio_id] = radio
        
    def get_state(self, curr_radio_id, action):
        state = 0
        curr_tx_pow = self.radios[curr_radio_id].get_tx_pow_step()
        link_status = self.radios[curr_radio_id].get_link_status()
        
        if action == Actions.NO_TX:
            return ExtPowerStates.RADIO_OFF

        if link_status == True:        
            if (curr_radio_id == 0):
                if (curr_tx_pow == 0):
                    state = ExtPowerStates.RADIO_0_0
                elif (curr_tx_pow == 1):
                    state = ExtPowerStates.RADIO_0_1
                elif (curr_tx_pow == 2):
                    state = ExtPowerStates.RADIO_0_2
                elif (curr_tx_pow == 2):
                    state = ExtPowerStates.RADIO_0_3
                elif (curr_tx_pow == 3):
                    state = ExtPowerStates.RADIO_0_4
            elif (curr_radio_id == 1):
                if (curr_tx_pow == 0):
                    state = ExtPowerStates.RADIO_1_0
                elif (curr_tx_pow == 1):
                    state = ExtPowerStates.RADIO_1_1
                elif (curr_tx_pow == 2):
                    state = ExtPowerStates.RADIO_1_2
                elif (curr_tx_pow == 2):
                    state = ExtPowerStates.RADIO_1_3
                elif (curr_tx_pow == 3):
                    state = ExtPowerStates.RADIO_1_4
            else:
                return 0
        else:
            if (curr_radio_id == 0):
                if (curr_tx_pow == 0):
                    state = ExtPowerStates.RADIO_0_0_DN
                elif (curr_tx_pow == 1):
                    state = ExtPowerStates.RADIO_0_1_DN
                elif (curr_tx_pow == 2):
                    state = ExtPowerStates.RADIO_0_2_DN
                elif (curr_tx_pow == 2):
                    state = ExtPowerStates.RADIO_0_3_DN
                elif (curr_tx_pow == 3):
                    state = ExtPowerStates.RADIO_0_4_DN
            elif (curr_radio_id == 1):
                if (curr_tx_pow == 0):
                    state = ExtPowerStates.RADIO_1_0_DN
                elif (curr_tx_pow == 1):
                    state = ExtPowerStates.RADIO_1_1_DN
                elif (curr_tx_pow == 2):
                    state = ExtPowerStates.RADIO_1_2_DN
                elif (curr_tx_pow == 2):
                    state = ExtPowerStates.RADIO_1_3_DN
                elif (curr_tx_pow == 3):
                    state = ExtPowerStates.RADIO_1_4_DN
            else:
                return 0
        
        return state
    
class MultiRadioSNRState():
    def __init__(self, radios):
        self.radios = radios
        
    def update_obs(self, radio_id, radio):
        self.radios[radio_id] = radio
        
    def get_state(self, curr_radio_id, actiom):
        state = 0
        snr = self.radios[curr_radio_id].get_snr()
        
        if curr_radio_id == 0:
            if snr <= 3.65:
                state = RadioSNRStates.RADIO_1_LINK_DN
            elif snr > 3.65 and snr <= 7:
                state = RadioSNRStates.RADIO_1_LOW
            elif snr > 7 and snr <= 13:
                state = RadioSNRStates.RADIO_1_LOW_MED
            elif snr > 13 and snr <=20:
                state = RadioSNRStates.RADIO_1_MED
            elif snr > 20 and snr <= 27:
                state = RadioSNRStates.RADIO_1_MED_HIGH
            elif snr > 27:
                state = RadioSNRStates.RADIO_1_HIGH
        elif curr_radio_id == 1:
            if snr <= 10:
                state = RadioSNRStates.RADIO_1_LINK_DN
            elif snr > 10 and snr <= 14:
                state = RadioSNRStates.RADIO_1_LOW
            elif snr > 14 and snr <= 20:
                state = RadioSNRStates.RADIO_1_LOW_MED
            elif snr > 20 and snr <=27:
                state = RadioSNRStates.RADIO_1_MED
            elif snr > 27 and snr <= 34:
                state = RadioSNRStates.RADIO_1_MED_HIGH
            elif snr > 34:
                state = RadioSNRStates.RADIO_1_HIGH
                
        
        return state
        