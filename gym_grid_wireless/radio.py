import numpy as np
from scipy.special import erfc
from gym_grid_wireless.errorRates import ser_awgn
from scipy import constants as const

k = 290
T = 1.38*10**-23


D_0 = 10

class Radio():

    def __init__(self, radio_id, freq, bw, rx_sensitivity, bit_rate, tx_power_steps, pwr_steps, mod_type='psk', mod_order='2'):
        '''
            freq = frequency in MHz
            bw = bandwidth in MHz
            rx_sensitivity = receiver sensitivity in dBm
            tx_power_steps = list of possible transmit powers (in dBm)
            pwr_steps = list of power consumptions (in mA) at each tx power step (must be same size)
        '''
        self.radio_id = radio_id
        self.freq = freq
        self.bw = bw
        self.rx_sensitivity = rx_sensitivity
        self.bit_rate = bit_rate
        self.tx_power_steps = tx_power_steps
        self.num_power_steps = len(self.tx_power_steps)
        self.pwr_steps = pwr_steps
        self.curr_tx_pow = 0
        self.mod_type = mod_type
        self.mod_order = mod_order
        
        self.RSSI_to_dst = 0                   # rssi in dBm
        self.fade_margin_to_dst = 0            # fade margin in dB
        self.link_status = 0
        self.shd_sigma = 0

        self.pwr_per_bit = []
        self.bits_per_ma = []
        self.bits_per_joule = []
        self.pow_W = []

        self.norm_bit_rate = 0
        self.norm_bits_per_joule = []
        self.norm_pwr_steps = []

        self.phy_mtu = 1500  # mtu in bytes
        self.phy_mtu_bits = self.phy_mtu*8

        for tx_power in self.tx_power_steps:
            self.pwr_per_bit.append(tx_power/self.bit_rate)

        for tx_power in pwr_steps:
            self.bits_per_ma.append(self.bit_rate/tx_power)
            self.pow_W.append((tx_power * 0.001) * 3.3) # power calculation assuming 3.3V transceiver

        # setup bits_per_joule
        
        for pwr in self.tx_power_steps:
            
            tx_pow_W = np.power(10,((pwr-30)/10))
            
            bpj = self.bit_rate/tx_pow_W
            self.bits_per_joule.append(bpj)

        self.noise_floor = (10*np.log10(k*T*bw))+30
        self.noise = -174

        self.ber_snr_ebn0_const = (10*np.log10(self.bw/self.bit_rate))
        
        
        self.lm = const.speed_of_light/(self.freq)
        self.K_const = 20*np.log10(self.lm/(4*const.pi*D_0))

    def get_pwr_ber_bit(self):
        return self.pwr_per_bit[self.curr_tx_pow]

    def get_bits_per_ma(self):
        return self.bits_per_ma

    def get_radio_id(self):
        return self.radio_id

    def get_freq(self):
        return self.freq

    def get_rx_sensitivity(self):
        return self.rx_sensitivity

    def get_pow_ma(self):
        return self.pwr_steps[self.curr_tx_pow]

    def get_pow_W(self):
        return self.pow_W[self.curr_tx_pow]

    def get_pow_W_at_step(self, tx_pow_step):
        return self.pow_W[tx_pow_step]

    def get_tx_pow(self):
        return self.tx_power_steps[self.curr_tx_pow]

    def get_tx_pow_step(self):
        return self.curr_tx_pow

    def set_tx_pow(self, tx_power_step):
        self.curr_tx_pow = tx_power_step

    def get_RSSI_to_dst(self):
        return self.RSSI_to_dst
    
    def set_RSSI_to_dst(self, rssi):
        self.RSSI_to_dst = rssi
    
    def set_norm_bit_rate(self, norm_br):
        self.norm_bit_rate = norm_br

    def get_norm_bit_rate(self):
        return self.norm_bit_rate

    def set_norm_bits_per_joule(self, norm_bpj):
        self.norm_bits_per_joule = norm_bpj

    def get_norm_bits_per_joule(self):
        return self.norm_bits_per_joule

    def set_norm_pwr_steps(self, norm_pwr_steps):
        self.norm_pwr_steps = norm_pwr_steps
    
    def get_norm_pwr_steps(self):
        return self.norm_pwr_steps
        
    def get_bits_per_joule(self):
        return self.bits_per_joule

    def get_fade_margin_to_dst(self):
        return self.fade_margin_to_dst

    def set_fade_margin_to_dst(self, margin):
        
        # only recalculate BER if our fade margin has actually changed...
        if margin != self.fade_margin_to_dst:
            self.fade_margin_to_dst = margin
            self.curr_ber = self.get_bit_error_rate()
        
        #self.curr_ber = self.get_bit_error_rate()
        
        #if (self.fade_margin_to_dst > 0) and (self.get_snr() > 0):
        #if (self.get_bit_error_rate() < 10**-5):
        #print('r ' + str(self.radio_id) + ' ' + str(self.prob_outage()))
        
        if (self.curr_ber > 10**-9):
            #print(self.get_bit_error_rate())
            self.link_status = 0
            #print(self.get_bit_error_rate())
        else:
            #print(self.get_bit_error_rate())
            self.link_status = 1

        '''
        if (self.prob_outage() > 0.3):
            self.link_status = 0
        if (self.prob_outage() > np.random.uniform()):
            self.link_status = 0
        else:
            self.link_status = 1
        '''
        '''
        if (self.fade_margin_to_dst > 0.0):
            self.link_status = 1
        else:
            self.link_status = 0
        '''

    def set_shd_sigma(self, shd):
        self.shd_sigma = shd

    def prob_outage(self):
        #print(self.shd_sigma)
        if (self.shd_sigma > 0):
            p_out = 1 - (0.5* erfc((1/np.sqrt(2)) * ((self.rx_sensitivity - self.RSSI_to_dst)/self.shd_sigma)))
        else:
            p_out = 0

        return p_out

    def get_link_status(self):
        return self.link_status

    def get_bit_rate(self):
        return self.bit_rate
    
    def get_noise_floor(self):
        return self.noise_floor
    
    def get_snr(self):
        
        if (self.noise_floor >= self.noise):
            snr = self.RSSI_to_dst - self.noise_floor
            
        else:
            #print('noise above floor' + str(self.noise) + ' ' + str(self.RSSI_to_dst - self.noise) + ' ' + str(self.get_freq()))
            snr = self.RSSI_to_dst - self.noise
        
        return snr

    def get_theo_bitrate(self):
        ebn0 = self.get_snr() + (10*np.log10(self.bw/self.bit_rate))
        br = (1-0.5*erfc(ebn0/np.sqrt(2)))*self.bit_rate
        #print(br)
        return br

    def get_bit_error_rate(self):
        #ebn0 = self.get_snr() + (10*np.log10(self.bw/self.bit_rate))
        ebn0 = self.get_snr() + self.ber_snr_ebn0_const
        #if (ebn0 < 20):
        #    print(ebn0, self.get_snr(), self.RSSI_to_dst, self.get_radio_id())
        #return 0.5*erfc(ebn0/np.sqrt(2))
        p_err = ser_awgn(np.array([ebn0]), self.mod_type, self.mod_order, 'coherent')
        #print(self.radio_id, p_err, ebn0, self.mod_type, self.get_snr(), self.get_fade_margin_to_dst(), self.noise_floor, self.RSSI_to_dst)
        return p_err
    
    def set_noise(self, noise):
        self.noise = noise

    # simulate small chunk of pkt transmission in specified time interval
    # returns number of bits "transmitted" and no. of bits in error (lost)
    def sim_pkt_data(self, pkt_size):
        
        if pkt_size > 0:
            #bytes_sec = self.get_bitrate()/8                    # no. of bytes per sec based on max bitrate
            #bytes_per_int = bytes_sec * time_interval           # bytes in this time interval
            #num_bits = bytes_per_int*8                          # bits in this time interval
            #num_bits = int(pkt_size * 8)                        # pkt_size is chunk size in bytes
            #p_err = self.get_bit_error_rate()                   # current bit error rate from current SNR
            #p_err = self.curr_ber
            p_err = 1 - np.power((1 - self.curr_ber), self.phy_mtu_bits)
            #print(p_err, self.curr_ber)
            
            if pkt_size >= self.phy_mtu:
                num_pkts = np.floor(pkt_size/self.phy_mtu)
            else:
                num_pkts = 1
            
            num_pkt_success = np.random.binomial(num_pkts, 1-p_err)
            #print(p_err, pkt_size, num_pkts, num_pkt_success)
            #if num_pkts != num_pkt_success:
            #    print('pkts lost ', num_pkt_success)
            '''
            bit_errors = 0                                      # tally of bit errors
            for i in range(0, num_bits):
                err = np.random.random()                        # bernoulli trial
                if err < p_err:                                 # if less than prob. of error then we have a errored bit...
                    bit_errors += 1
            '''
            #bit_errors = num_bits - (np.random.geometric(p=(1-p_err), size=num_bits) == 1).sum()
            #bit_errors = 0
            #if (bit_errors > 0):
                #print(self.radio_id, bit_errors, num_bits, p_err, self.get_snr())
            return num_pkts, num_pkts-num_pkt_success
        else:
            return 0, 0

