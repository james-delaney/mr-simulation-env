import numpy as np


# recording structure - dictionary of each paramter/data type to record - list of lists for results of each run
# data = {
#     'data_point_0': [[0,1,2,3,4,5,6],       # Run 1
#                      [1,2,3,4,5,6,7],       # Run 2
#                      [2,3,4,5,6,7,8]],      # Run 3
#     'data_point_1': [[0,1,2,3,4,5,6],       # Run 1 (data point 1)
#                      [1,2,3,4,5,6,7],       # Run 2 (data point 1)
#                      [2,3,4,5,6,7,8]],      # Run 3 (data point 1)
# }



class SimRecorder():

    def __init__(self):
        self.data = {}
        self.curr_run = -1

    def add_data_point(self, data_point_name):
        self.data[data_point_name] = []

    def add_cum_data_point(self, data_point_name):
        self.data[data_point_name] = []

    def record_data(self, data_point_name, data):
        self.data[data_point_name][self.curr_run].append(data)

    def record_cum_data(self, data_point_name, data):
        old = self.data[data_point_name][self.curr_run]
        old[0] = old[0] + data
        self.data[data_point_name][self.curr_run] = old

    def new_run(self):
        for name in self.data.keys():
            self.data[name].append([0])
        self.curr_run += 1
        
    def mean_across_runs(self, data_point_name):
        d = self.data[data_point_name]
        d = np.array(d)
        return np.mean(d), self.curr_run+1
    
    def std_across_runs(self, data_point_name):
        d = self.data[data_point_name]
        d = np.array(d)
        return np.std(d), self.curr_run+1
