'''
from gym.envs.registration import register

register(
    id='grid-wireless-v0',
    entry_point='gym_grid_wireless.envs:GridWirelessEnv',
)

register(
    id='linear-wireless-v0',
    entry_point='gym_grid_wireless.envs:LinearWirelessEnv',
)

register(
    id='path-wireless-v0',
    entry_point='gym_grid_wireless.envs:PathWirelessEnv',
)

register(
    id='path-power-wireless-v0',
    entry_point='gym_grid_wireless.envs:PathPowerWirelessEnv',
)


register(
    id='path-power-wireless-v1',
    entry_point='gym_grid_wireless.envs:PathPowerWirelessEnvV1',
)

register(
    id='path-power-wireless-v2',
    entry_point='gym_grid_wireless.envs:PathPowerWirelessEnvV2',
)

register(
    id='path-power-wireless-v3',
    entry_point='gym_grid_wireless.envs:PathPowerWirelessEnvV3',
)

register(
    id='path-power-int-wireless-v0',
    entry_point='gym_grid_wireless.envs:PathPowerIntWirelessEnv',
)
register(
    id='meas-wireless-v0',
    entry_point='gym_grid_wireless.envs:MeasWirelessEnv',
)
'''









'''
register(
    id='grid-wireless-extrahard-v0',
    entry_point='gym_grid_wireless.envs:GridWirelessExtraHardEnv',
)
'''